#!/usr/bin/env bash
if [ "$#" -ne 1 ]; then
  echo "Usage : ${0} <name> <pkg 1> <pkg 2> ..."
  exit 0
fi

django-admin startproject --template=~/PycharmProjects/new_drf_project/new.zip $1
virtualenv ~/PycharmProjects/$1/venv/ -p $(which python3.6)
source ~/PycharmProjects/$1/venv/bin/activate
cd ~/PycharmProjects/$1/
touch .env
pip install -r requirements.txt
argc=$#
argv=($@)
for (( j=1; j<argc; j++ )); do
    pip install ${argv[j]}
done
pip freeze > requirements.txt

python manage.py makemigrations
python manage.py migrate

git init
git add *
git add .gitignore
git commit -m "First commit"
