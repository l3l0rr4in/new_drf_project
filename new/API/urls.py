from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

from API.views import *


router = SimpleRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
]