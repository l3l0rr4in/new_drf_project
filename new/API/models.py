from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from utils import validate_file_extension

# Create your models here.


class MyUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
