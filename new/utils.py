import os
import yaml
import datetime
from django.core.exceptions import ValidationError


def true_or_false(string):
    if isinstance(string, str):
        return bool(yaml.load(string, Loader=yaml.FullLoader))
    else:
        return False


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf', '.doc', '.docx', '.jpg', '.jpeg', '.png', '.xlsx', '.xls', '.csv']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')


def time_plus(time, timedelta):
    start = datetime.datetime(
        2000, 1, 1,
        hour=time.hour, minute=time.minute, second=time.second)
    end = start + timedelta
    return end.time()


def set_export_directory(name):
    if not os.path.exists("./media/" + name):
        os.makedirs("./media/" + name)
    os.chdir("./media/" + name)
